#!/bin/bash
function usage {
    echo 'Slow_loris_Detect :' 
    echo 'usage: Slow_loris_Detect [ options ] '
    echo '-h --help  show this help message'
    echo '-m --max-connections  : maximum connections for an ip address (default is 100)' 
    echo '-b --block-address  adr : block the ip address adr'
    echo '--keep-log-files  : log files are deleted by default (/tmp/detect_loris.log,/tmp/guilty_address.log)'
    echo ''
    exit
}

options=`getopt -o m:b:h -l max-connections:  -l help -l block-address: -l keep-log-files -- "$@" `


MAX=100
DELETE_LOG=true
case "$1" in
    -m | --max-connections ) MAX=$2;shift 2;;
    -b | --block-address ) adr=$2;shift 2;
			   ip rule add blackhole from $adr;;
    # or iptables -I INPUT -s $adr -j DROP 
    -h | --help ) usage;;
    --keep-log-files ) DELETE_LOG=false; shift;;
    -- ) shift ;break ;;
esac


echo "This file stores all ip addresses that are connected to the host (peer address)" >>/tmp/detect_loris.log 

# getting all ip address that are connected to the host
# tail -n +2 : to delete the first line of 'ss -t'

ss  -4t | tail -n +2 |awk '{print  $5}' |while read line
#netstat | tail -n +2 |awk '{print  $5}' |while read line

do
    # storing the ip address and deleting the port number
    echo ${line%:*} >>/tmp/detect_loris.log 
done


echo "This file stores ip addresses  that are occupying more than $MAX connections" >>/tmp/guilty_address.log

# counting  connections per single address
tail -n +2 /tmp/detect_loris.log |sort  | uniq -c | while read count address
do
    if (( count>=MAX ))
    then
	echo "Warning : more then $MAX connections are occupied by a single ip address $address"
	echo $address >>/tmp/guilty_address.log
	read  -p  "do you want to block the address : $address (y/n)" yes_no  </dev/tty
	# read
	case $yes_no in
	#if [[ $yes_no==y ]] ;
	#then
	    #	    ip rule add blackhole from $address
	y|Y|yes)    route add -host $address reject
		    echo "address  $address is blocked ";;
	esac
	#fi
	    
    fi
       
done					     
read -p "press ENTER to quit"
if $DELETE_LOG is true
then
    rm /tmp/guilty_address.log
    rm /tmp/detect_loris.log
fi
